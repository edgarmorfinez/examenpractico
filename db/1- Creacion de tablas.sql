USE [DB_RecetarioWeb]
GO

/****** Object:  Table [dbo].[[TBL_Users]]    Script Date: 10/03/2022 17:11:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TBL_Users](
	[PK_UserId] [int] IDENTITY(1,1) NOT NULL,
	[rctName] [varchar](150) NULL,
	[rctAditional] [varchar](128) NULL,
	[rctPassword] [varchar](254) NULL,
	[rctEmail] [varchar](250) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UserId] PRIMARY KEY CLUSTERED 
(
	[PK_UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
)
GO


