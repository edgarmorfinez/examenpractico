USE [DB_RecetarioWeb]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Consulta de la receta
-- =============================================
CREATE OR ALTER PROCEDURE CreateUsers
@Name AS VARCHAR(150),
@Aditional AS VARCHAR(150),
@Password VARCHAR(200),
@Email AS VARCHAR(150)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO TBL_Users (rctName, rctAditional, rctPassword, rctEmail, CreateDate, UpdateDate)
	VALUES (@Name, @Aditional, @Password, @Email, GETDATE(), GETDATE())
END
GO





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Consulta de usuario
-- =============================================
CREATE OR ALTER PROCEDURE GetUserId
@UserId AS INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT PK_UserId, rctName, rctAditional, rctPassword
	FROM TBL_Users
	WHERE PK_UserId = @UserId
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Consulta de usuario
-- =============================================
CREATE OR ALTER PROCEDURE GetUserEmail
@Email AS VARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT PK_UserId, rctName, rctAditional, rctPassword, rctEmail, CreateDate, UpdateDate
	FROM TBL_Users
	WHERE rctEmail = @Email
END
GO






SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Registro de receta
-- =============================================
CREATE OR ALTER PROCEDURE CreateCookingRecipes
@Name AS VARCHAR(150),
@Description VARCHAR(200),
@Cooking AS VARCHAR(MAX),
@Image AS VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO TBL_CookingRecipes (rctName, rctCooking, rctDescription, rctImage, CreateDate, UpdateDate)
	VALUES (@Name, @Cooking, @Description, @Image, GETDATE(), GETDATE())
END
GO






SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Borrado de receta
-- =============================================
CREATE OR ALTER PROCEDURE DeleteCookingRecipes
@CookingRecipesId AS INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE TBL_CookingRecipes
	WHERE PK_CookingRecipeId = @CookingRecipesId
END
GO







SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Obtiene recetas
-- =============================================
CREATE OR ALTER PROCEDURE GetCookingRecipes
AS
BEGIN
	SET NOCOUNT ON;

	SELECT PK_CookingRecipeId, rctName, rctCooking, rctDescription, rctImage, CreateDate, UpdateDate
	FROM TBL_CookingRecipes
	ORDER BY CreateDate DESC
END
GO





SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Consulta de la receta
-- =============================================
CREATE OR ALTER PROCEDURE GetCookingRecipesPorId
@CookingRecipesId AS INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT PK_CookingRecipeId, rctName, rctCooking, rctDescription, rctImage, CreateDate, UpdateDate
	FROM TBL_CookingRecipes
	WHERE PK_CookingRecipeId = @CookingRecipesId
	ORDER BY CreateDate DESC
END
GO









SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Obtiene recetas para la portada
-- =============================================
CREATE OR ALTER PROCEDURE GetPortadasCookingRecipes
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 5 PK_CookingRecipeId, rctName, rctDescription, rctCooking, rctImage, CreateDate, UpdateDate
	FROM TBL_CookingRecipes
	ORDER BY CreateDate DESC
END
GO








SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edgar Morfinez
-- Create date: Marzo 2022
-- Description:	Actualiza la receta
-- =============================================
CREATE OR ALTER PROCEDURE UpdateCookingRecipes
@CookingRecipesId AS INT,
@Name AS VARCHAR(150),
@Description AS VARCHAR(200),
@Cooking AS VARCHAR(MAX),
@Image AS VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE TBL_CookingRecipes
	SET rctName = @Name, 
		rctCooking = @Cooking,
		rctDescription = @Description,
		rctImage = @Image, 
		UpdateDate = GETDATE()
	WHERE PK_CookingRecipeId = @CookingRecipesId
END
GO
