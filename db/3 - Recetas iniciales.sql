USE [DB_RecetarioWeb]
GO

INSERT INTO [dbo].[TBL_CookingRecipes]
           ([rctName]
           ,[rctDescription]
           ,[rctCooking]
           ,[rctImage]
           ,[CreateDate]
           ,[UpdateDate])
     VALUES
           ('DELICIOSAS TOSTADAS DE JAIBA'
           ,'LA CUARESMA YA COMENZ�: EMPIEZA CON BROCHE DE ORO PROBANDO ESTAS DELICIOSAS TOSTADAS DE JAIBA. �TE ENCANTAR� SU IRRESISTIBLE SABOR!'
           ,'Mezcla la jaiba con el jitomate, el perejil, el chile, el jugo de lim�n, la catsup y la sal; deja marinar 30 minutos en el refrigerador; retira el l�quido y rectifica la saz�n.Unta mayonesa en las to'
           ,'https://dam.cocinafacil.com.mx/wp-content/uploads/2020/04/tostadas-de-jaiba.jpg'
		   ,GETDATE()
           ,GETDATE())
GO



INSERT INTO [dbo].[TBL_CookingRecipes]
           ([rctName]
           ,[rctDescription]
           ,[rctCooking]
           ,[rctImage]
           ,[CreateDate]
           ,[UpdateDate])
     VALUES
           ('CHILES CUARESME�OS CON PICADILLO'
           ,'SIRVE UNA DELICIOSA BOTANA PREPARADA EN CASA CON ESTOS CHILES CUARESME�OS CON PICADILLO. TE DAMOS EL PASO A PASO PARA PREPARARLOS.'
           ,'Haz un corte a lo largo de los chiles, qui�tales las semillas y coci�nalos en agua hirviendo con sal hasta que este�n cocidos, pero firmes (cambian de color y se opacan); reserva.Aparte, cocina las p'
           ,'https://dam.cocinafacil.com.mx/wp-content/uploads/2019/01/chiles-cuaresmenos-con-picadillo.jpg'
		   ,GETDATE()
           ,GETDATE())
GO



INSERT INTO [dbo].[TBL_CookingRecipes]
           ([rctName]
           ,[rctDescription]
           ,[rctCooking]
           ,[rctImage]
           ,[CreateDate]
           ,[UpdateDate])
     VALUES
           ('CROQUETAS DE AT�N'
           ,'HAZ UNAS CROQUETAS DE AT�N PARA QUE NO PASES TODA LA TARDE EN LA COCINA. �ESTA RECETA ES FACIL�SIMA! VAS A TERMINAR ENAMORADA DE ELLA.'
           ,'Sofri�e la cebolla y el ajo con el aceite de oliva. Cuando comiencen a dorar, agrega el jitomate. Cuece a fuego bajo hasta que se consuma todo el li�quido; salpimienta.Mezcla el sofrito con el pure� '
           ,'https://dam.cocinafacil.com.mx/wp-content/uploads/2020/04/croquetas-de-atun.jpg'
		   ,GETDATE()
           ,GETDATE())
GO



INSERT INTO [dbo].[TBL_CookingRecipes]
           ([rctName]
           ,[rctDescription]
           ,[rctCooking]
           ,[rctImage]
           ,[CreateDate]
           ,[UpdateDate])
     VALUES
           ('ROLLOS DE BISTEC CON MANGO EN SALSA DE CHILE ANCHO'
           ,'TE ENAMORAR�S DE LA COMBINACI�N DE SABORES QUE TE OFRECEN ESTOS ROLLOS DE BISTEC CON MANGO EN SALSA DE CHILE ANCHO. �MUY F�CILES DE PREPARAR!'
           ,'Sazona los bisteces con sal y pimienta, reparte las tiras de mango entre ellos, enro�llalos y atora con palillos de madera.Calienta un poco de aceite en una sarte�n y fri�e los rollos hasta que este�'
           ,'https://dam.cocinafacil.com.mx/wp-content/uploads/2019/01/rollo-bistec-mango.jpg'
		   ,GETDATE()
           ,GETDATE())
GO

