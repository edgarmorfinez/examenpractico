﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PublicisDigitasBack.Models;

namespace PublicisDigitasBack.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CookingRecipesController : Controller
    {
        private readonly RecetarioWebContext _context;

        public CookingRecipesController(RecetarioWebContext context)
        {
            _context = context;
        }

        #region Metodos publicos

        [HttpGet("Portada")]
        [AllowAnonymous]
        public IActionResult Index()
        {
            var portada = ObtenerPortadas().Take(4).ToList();

            if(portada == null || portada.Count < 4)
            {
                return NotFound("No se encontraron recetas");
            }

            return Json(portada, new JsonSerializerOptions
            {
                WriteIndented = true
            });
        }

        [HttpGet("Recetas")]
        [Authorize]
        public IActionResult Recetas()
        {
            var portada = ObtenerRecetas();

            if (portada == null || !portada.Any())
            {
                return NotFound("No se encontraron recetas");
            }

            return Json(portada, new JsonSerializerOptions
            {
                WriteIndented = true
            });
        }


        [HttpGet("Details")]
        [Authorize]
        public IActionResult Details(int id)
        {
            if (id == 0)
            {
                return BadRequest("No se proporciono la clave de la receta");
            }

            var tblCookingRecipes = GetCookingRecipesPorId(id);

            if (tblCookingRecipes == null || !tblCookingRecipes.Any())
            {
                return NotFound("No se encontro la receta con la clave proporcionada");
            }

            return Json(tblCookingRecipes, new JsonSerializerOptions
            {
                WriteIndented = true
            });
        }

        [HttpPost("Create")]
        [Authorize]
        public  IActionResult Create([Bind("RctName,RctDescription,RctCooking,RctImage")] Tbl_CookingRecipes tblCookingRecipes)
        {
            if (ModelState.IsValid)
            {
                if (CreateCookingRecipes(tblCookingRecipes) == 0)
                {
                    return BadRequest();
                }

                return Ok();
            }

           return BadRequest("Modelo invalido");
        }

        [HttpPost("Edit")]
        [Authorize]
        public IActionResult Edit(int id, [Bind("Pk_CookingRecipeId,RctName,RctDescription,RctCooking,RctImage")] Tbl_CookingRecipes tblCookingRecipes)
        {
            if(id == 0)
            {
                return BadRequest("No se proporciono la clave de la receta");
            }

            if (id != tblCookingRecipes.Pk_CookingRecipeId)
            {
                return NotFound("No se encontro la receta con la clave proporcionada");
            }

            if (ModelState.IsValid)
            {
                if (!GetCookingRecipesPorId(tblCookingRecipes.Pk_CookingRecipeId).Any())
                {
                    return BadRequest();
                }

                UpdateCookingRecipes(tblCookingRecipes);
                return Ok();                
            }

            return BadRequest("Modelo invalido");
        }

        [HttpPost("Delete")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("No se proporciono la clave de la receta");
            }

            var tblCookingRecipes = GetCookingRecipesPorId(id);

            if (tblCookingRecipes  == null || !tblCookingRecipes.Any())
            {
                return NotFound("No se encontro la receta con la clave proporcionada");
            }

            if ( DeleteCookingRecipes(id) == 0)
            {
                return BadRequest();
            }

            return Ok();
        }

        #endregion

        #region Metodos privados

        private List<Tbl_CookingRecipes> ObtenerPortadas()
        {
            List<Tbl_CookingRecipes> portadas;

            try
            {
                portadas = _context.Tbl_CookingRecipes.FromSqlRaw("EXEC GetPortadasCookingRecipes").ToList();
            }catch(Exception)
            {
                return null;
            }

            return portadas;
        }

        private List<Tbl_CookingRecipes> ObtenerRecetas()
        {
            List<Tbl_CookingRecipes> portadas;

            try
            {
                portadas = _context.Tbl_CookingRecipes.FromSqlRaw("EXEC GetCookingRecipes").ToList();
            }catch(Exception)
            {
                return null;
            }

            return portadas;
        }
       
        private List<Tbl_CookingRecipes> GetCookingRecipesPorId(int id)
        {
            List<Tbl_CookingRecipes> portadas;

            try { 
                portadas = _context.Tbl_CookingRecipes.FromSqlRaw("EXEC GetCookingRecipesPorId {0}", id).ToList();
            }catch(Exception)
            {
                return null;
            }

            return portadas;
        }

        private int CreateCookingRecipes(Tbl_CookingRecipes tblCookingRecipes)
        {
            return _context.Database.ExecuteSqlRaw("EXEC CreateCookingRecipes {0}, {1}, {2}, {3}", 
                 tblCookingRecipes.RctName, tblCookingRecipes.RctCooking, tblCookingRecipes.RctDescription, tblCookingRecipes.RctImage);
        }

        private int UpdateCookingRecipes(Tbl_CookingRecipes tblCookingRecipes)
        {
            return _context.Database.ExecuteSqlRaw("EXEC UpdateCookingRecipes {0}, {1}, {2}, {3}, {4}",
                tblCookingRecipes.Pk_CookingRecipeId, tblCookingRecipes.RctName, tblCookingRecipes.RctCooking, tblCookingRecipes.RctDescription, 
                tblCookingRecipes.RctImage);
        }

        private int DeleteCookingRecipes(int id)
        {
            return _context.Database.ExecuteSqlRaw("EXEC DeleteCookingRecipes {0}", id);
        }

        #endregion

    }
}
