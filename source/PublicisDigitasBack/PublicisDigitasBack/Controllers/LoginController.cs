﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PublicisDigitasBack.Models;
using PublicisDigitasBack.Models.DTO;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace PublicisDigitasBack.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {
        private readonly RecetarioWebContext _context;
        protected readonly IConfiguration _config;

        public LoginController(RecetarioWebContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        [HttpPost("Create")]
        [AllowAnonymous]
        public ActionResult Create(LoginDTO loginDTO)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var usuario = ObtenerUsuarioId(loginDTO);

                    if (usuario.Any())
                    {
                        return BadRequest("Usuario registrado previamente");
                    }

                    string adicional = GetSalt();
                    Tbl_Users user = new Tbl_Users
                    {
                        RctEmail =  loginDTO.Email,
                        RctName = loginDTO.Name,
                        RctAditional = adicional,
                        RctPassword = GetHash(adicional + loginDTO.Password)
                    };
                      
                    if( CreateUsers(user) == 0)
                    {
                        return BadRequest();
                    }

                    return Ok();
                }
                catch (Exception)
                {
                    return BadRequest();    
                }
            }

            return BadRequest("Modelo invalido");
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public ActionResult Login(LoginDTO loginDTO)
        {
            TokenDTO tokenDTO = new TokenDTO();
            var usuario = ObtenerUsuarioId(loginDTO);

            if (usuario == null || !usuario.Any())
            {
                return BadRequest("Usuario no encontrado");
            }

            if (usuario[0].RctPassword != GetHash(usuario[0].RctAditional + loginDTO.Password))
            {
                return BadRequest("Contraseña incorrecta");
            }

            DateTime fechaExpiracion = DateTime.Now.AddMinutes(60).ToLocalTime();
            tokenDTO.Token = GenerarToken(fechaExpiracion);
            tokenDTO.TokenExpiration = fechaExpiracion;
            tokenDTO.UsuarioId = usuario[0].PK_UserId;
            tokenDTO.UsuarioName = usuario[0].RctName;

            return Json(tokenDTO, new JsonSerializerOptions
            {
                WriteIndented = true
            });
        }

        #region Metodos privados

        private int CreateUsers(Tbl_Users users)
        {
            return _context.Database.ExecuteSqlRaw("EXEC CreateUsers {0}, {1}, {2}, {3}",
                 users.RctName, users.RctAditional, users.RctPassword, users.RctEmail);
        }


        private List<Tbl_Users> ObtenerUsuarioId(LoginDTO loginDTO)
        {
            List<Tbl_Users> usuario;

            try
            {
                usuario = _context.TblUsers.FromSqlRaw("EXEC GetUserEmail {0}", loginDTO.Email).ToList();
            }
            catch (Exception)
            {
                return null;
            }

            return usuario;
        }

        #endregion

        #region Seguridad

        private string GetSalt()
        {
            byte[] bytes = new byte[128 / 8];
            using var keyGenerator = RandomNumberGenerator.Create();
            keyGenerator.GetBytes(bytes);
            return BitConverter.ToString(bytes).ToLower();
        }

        private string GetHash(string text)
        {
            using var sha256 = SHA256.Create();
            var hashedBytes = sha256
                    .ComputeHash(Encoding.UTF8.GetBytes(text));
          
            return BitConverter
                 .ToString(hashedBytes).ToLower();
        }

        public string GenerarToken(DateTime fechaExpiracion)
        {
            var key = new SymmetricSecurityKey
                       (Encoding.UTF8.GetBytes(_config["Tokens:Key"]));
            var creds = new SigningCredentials
                       (key, SecurityAlgorithms.HmacSha256);
            JwtSecurityToken jwtToken = new JwtSecurityToken
                       (_config["Tokens:Issuer"],
                        _config["Tokens:Issuer"],
                        expires: fechaExpiracion,
                        signingCredentials: creds);
            string token = new JwtSecurityTokenHandler()
                        .WriteToken(jwtToken);

            return token;
        }

        #endregion

    }
}
