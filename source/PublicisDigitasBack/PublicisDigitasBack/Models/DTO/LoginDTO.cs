﻿namespace PublicisDigitasBack.Models.DTO
{
    public class LoginDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
