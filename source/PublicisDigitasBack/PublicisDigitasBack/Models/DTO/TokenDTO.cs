﻿using System;

namespace PublicisDigitasBack.Models
{
    public class TokenDTO
    {
        public string Token { get; set; }

        public DateTime TokenExpiration { get; set; }

        public int UsuarioId { get; set; }

        public string UsuarioName { get; set;}
    }
}
