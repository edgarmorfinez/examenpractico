﻿using Microsoft.EntityFrameworkCore;

namespace PublicisDigitasBack.Models
{
    public partial class RecetarioWebContext : DbContext
    {
        public RecetarioWebContext(DbContextOptions<RecetarioWebContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tbl_CookingRecipes> Tbl_CookingRecipes { get; set; }
        public virtual DbSet<Tbl_Ingredient> TblIngredient { get; set; }
        public virtual DbSet<Tbl_Users> TblUsers { get; set; }
    }
}
