﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicisDigitasBack.Models
{
    public class Tbl_CookingRecipes
    {
        [Key]
        public int Pk_CookingRecipeId { get; set; }
        public string RctName { get; set; }
        public string RctDescription { get; set; }
        public string RctCooking { get; set; }
        public string RctImage { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

    }
}
