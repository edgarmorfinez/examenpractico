﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicisDigitasBack.Models
{
    public class Tbl_Ingredient
    {
        [Key]
        public int Pk_IngredientId { get; set; }
        public int? Fk_CookingRecipeId { get; set; }
        public string IngtName { get; set; }
        public string IngtPortion { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

    }
}
