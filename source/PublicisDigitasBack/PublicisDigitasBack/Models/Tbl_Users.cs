﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicisDigitasBack.Models
{
    public class Tbl_Users
    {
		[Key]
		public int PK_UserId { get; set; }
		public string RctName { get; set; }
		public string RctAditional { get; set; }
		public string RctPassword { get; set; }
		public string RctEmail { get; set; }		
		public DateTime? CreateDate { get; set; }
		public DateTime? UpdateDate { get; set; }
	}
}
