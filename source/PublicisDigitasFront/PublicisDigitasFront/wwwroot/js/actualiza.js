﻿

$(document).ready(function () {
    cargaReceta()

    $("#actualizaReceta").submit(function (e) {
        e.preventDefault();
        var valores = $(this);

        ActualizaReceta(valores)
    });
});


function cargaReceta() {

    var recipeid = getParameterByName('id');

    $("#CookingRecipeId").val(recipeid);

    $.ajax({
        url: rutaServicio + "CookingRecipes/Details?id=" + recipeid,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: true,
        headers: {
            accept: "application/json",
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + localStorage.getItem("BearerToken")
        }
    })
        .done(function (respuesta) {
            if ((respuesta != null && respuesta != "[]")) {
                $("#RctName").val(respuesta[0].RctName);
                $("#RctDescription").val(respuesta[0].RctDescription);
                $("#RctCooking").val(respuesta[0].RctCooking);
                $("#RctImage").val(respuesta[0].RctImage);            
            } else {
                sweetalerts('Error', 'Error al contactar el servidor', 'info', 'Aceptar')
            }
        })
        .fail(function (xhr, status, error) {
            validFail(xhr, status, error)
        })
        .always(function () {
        })
}

function ActualizaReceta() {
    var recipeid = Number($("#CookingRecipeId").val());
    var receta = {        
        Pk_CookingRecipeId: recipeid,
        RctName: $("#RctName").val(),
        RctDescription: $("#RctDescription").val(),
        RctCooking: $("#RctCooking").val(),
        RctImage: $("#RctImage").val()
    };

    swal({
        title: "Confirmacion",
        text: "¿Confirma la actualización de la receta?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: rutaServicio + "CookingRecipes/Edit?id=" + recipeid,
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                    data: JSON.stringify(receta),
                    headers: {
                        accept: "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Authorization": "Bearer " + localStorage.getItem("BearerToken")
                    }
                })
                    .done(function (respuesta) {
                        if ((respuesta != null && respuesta != "[]")) {
                            sweetalertRedirect('Mensaje', 'Registro actualizado correctamente.', 'success', 'Aceptar', '../Administra/Informacion');
                        } else {
                            sweetalerts('Mensaje', 'Error al contactar el servidor', 'info', 'Aceptar');
                        }
                    })
                    .fail(function (xhr, status, error) {
                        validFail(xhr, status, error)
                    })
                    .always(function () {
                    })
            } else {
                return;
            }
        });




}
