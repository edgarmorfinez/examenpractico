﻿

$(document).ready(function () {
    cargaRecetas()
});

function cargaRecetas() {
    $.ajax({
        url: rutaServicio + "CookingRecipes/Recetas",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: true,
        headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + localStorage.getItem("BearerToken")
        }
    })
        .done(function (respuesta) {
            if ((respuesta != null && respuesta != "[]")) {

                var tabla = $('#tblRecetas').DataTable({
                    "data": respuesta,
                    "columnDefs": [
                        {
                            'targets': 0,
                            'className': 'dt-cell-right',
                            'width' : '40px'
                        },
                        {
                            'targets': 1,
                            'className': 'dt-cell-left  limite-superado limite-nombre',
                            'width': '150px',
                            'max-width': '150px',
                        },
                        {
                            'targets': 2,
                            'className': 'dt-cell-left  limite-superado limite-descripcion',
                            'width': '250px',
                            'max-width': '250px'
                        },
                        {
                            'targets': 3,
                            'className': 'dt-cell-left',
                            'width': '70px',
                            'render': function (data, type, full, meta) {
                                var x = new Date(full.CreateDate)
                                return x.getDate() + "/" + (x.getMonth() + 1) + "/" + x.getFullYear();
                            }
                        },
                        {
                            'targets': 4,
                            'className': 'dt-cell-left',
                            'width': '70px',
                            'render': function (data, type, full, meta) {
                                var x = new Date(full.UpdateDate)
                                return x.getDate() + "/" + (x.getMonth() + 1)  + "/" + x.getFullYear();
                            }
                        },
                        {
                            'targets': 5,
                            'width': '80px',
                            'render': function (data, type, full, meta) {
                                return '<button  type="button" class="btn-table btn btn-primary" href="javascript:;" onclick="Edita(' + full.Pk_CookingRecipeId + ')"> Editar </button>';
                            }
                        },
                        {
                            'targets': 6,
                            'width': '80px',
                            'render': function (data, type, full, meta) {
                                return '<button  type="button" class="btn-table btn  btn-danger" href="javascript:;" onclick="Elimina(' + full.Pk_CookingRecipeId + ')"> Eliminar </button>';
                            }
                        }
                    ],
                    "columns": [
                        { 'data': 'Pk_CookingRecipeId' },
                        { 'data': 'RctName' },
                        { 'data': 'RctDescription' },
                        { 'data': 'CreateDate' },
                        { 'data': 'UpdateDate' },
                    ],
                    "order": [[0, "desc"]],
                    "searching": false,
                    "paging": false,
                    "info": false,
                    'bLengthChange': false,
                    'bFilter': false,
                    'bSort': false,
                    'bPaginate': false,
                    'bAutoWidth': true,
                    'sScrollX': '100%',
                    'bScrollColapse': true,
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por pagina",
                        "zeroRecords": "Sin informacion",
                        "info": "Mostrar pagina _PAGE_ de _PAGES_",
                        "infoEmpty": "Ningun registro encontrado",
                        "infoFiltered": "filtrado de _MAX_ registros totales",
                        "search": "Buscar",
                        "loadingRecords": "Cargando registros",
                        "processing": "Procesando",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    }
                });
            } else {
                sweetalerts('Error', 'Error al contactar el servidor', 'info', 'Aceptar')
            }
        })
        .fail(function (xhr, status, error) {
            validFail(xhr, status, error)
        })
        .always(function () {
        })
}

function Edita(id) {
    window.location = "../Administra/Actualiza?id=" + id;
}

function Elimina(RecipeId) {

    swal({
        title: "Confirmacion",
        text: "¿Confirma eliminar la receta?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: rutaServicio + "CookingRecipes/Delete?id=" + RecipeId,
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                    headers: {
                        accept: "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Authorization": "Bearer " + localStorage.getItem("BearerToken")
                    }
                })
                    .done(function (respuesta) {
                        if ((respuesta != null && respuesta != "[]")) {
                            sweetalertRedirect('Mensaje', 'Registro eliminado correctamente.', 'success', 'Aceptar', '../Administra/Informacion');
                        } else {
                            sweetalerts('Mensaje', 'Error al contactar el servidor', 'info', 'Aceptar');
                        }
                    })
                    .fail(function (xhr, status, error) {
                        validFail(xhr, status, error)
                    })
                    .always(function () {
                    })
            } else {
                return;
            }
        });



   
}
