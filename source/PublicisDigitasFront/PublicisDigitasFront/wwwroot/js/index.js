﻿

$(document).ready(function () {

    cargaPortada()

});

function cargaPortada() {
    $.ajax({
        url: "http://localhost:5600/CookingRecipes/Portada",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: true,
        headers: {
            accept: "application/json",
            "Access-Control-Allow-Origin": "*"
        }
    })
        .done(function (respuesta) {
            if ((respuesta != null && respuesta != "[]")) {

                $('#descripcionPrincipal').text(respuesta[0].RctCooking)
                $('#tituloPrincipal').text(respuesta[0].RctName)
                $('#imagenPrincipal').prop('src', respuesta[0].RctImage)

                for (var contador = 1; contador <= 3; contador++) {
                    $('#tituloCarosel' + contador).text(respuesta[contador].RctName)
                    $('#imagenCarosel' + contador).prop('src', respuesta[contador].RctImage)
                }

                $("#portada").removeClass("d-none");
                $("#caroselDimensions").removeClass("d-none");
            } else {
                sweetalerts('Advertencia', 'No se pudo cargar datos de portada', 'info', 'Aceptar')
                $("#portada").addClass("d-none");
                $("#caroselDimensions").addClass("d-none");
            }
        })
        .fail(function (xhr, status, error) {
            $("#portada").addClass("d-none");
            $("#carosel-dimensions").addClass("d-none");

            validFail(xhr, status, error)
        })
        .always(function () {
        })
}

