﻿$(document).ready(function () {

    $("#registra").submit(function (e) {
        Registra(e)
    });

    $("#login").submit(function (e) {
        IniciaSesion(e)
    });

});

function IniciaSesion(e) {
    e.preventDefault(); 

    var login = {
        Email : $("#email_address").val(),
        Password : $("#password").val()
    };

    $.ajax({
        url: rutaServicio + "Login/Login",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        async: true,
        data: JSON.stringify(login),
        headers: {
            accept: "application/json",
            "Access-Control-Allow-Origin": "*"
        }
     })
        .done(function (respuesta) {
            if ((respuesta != null && respuesta != "[]")) {
                localStorage.setItem("BearerToken", respuesta.Token)
                localStorage.setItem("userName", respuesta.UsuarioName)
                window.location = "../Administra/Informacion";
            } else {
                sweetalerts("Adevertencia", 'No se pudo validar el usuario', 'warning', 'Aceptar');
            }
        })
        .fail(function (xhr, status, error) {
            validFail(xhr, status, error)
        })
        .always(function () {
        })

}

function Registra(e) {
    e.preventDefault();

    if ($("#password").val() != $("#re-password").val()) {
        sweetalerts('Login', 'Contraseñas ingresadas no coinciden', 'warning', 'Aceptar');
        return;
    }

    var registra = {
        Name: $("#name").val(),
        Email: $("#email_address").val(),
        Password: $("#password").val()
    };

    $.ajax({
        url: rutaServicio + "Login/Create",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        async: true,
        data: JSON.stringify(registra),
        headers: {
            accept: "application/json",
            "Access-Control-Allow-Origin": "*"
        }
    })
        .done(function (respuesta) {
            sweetalertRedirect('Login', 'Se registro el usuario correctamente', 'success', 'Aceptar', '../Login/IniciaSesion')
        })
        .fail(function (xhr, status, error) {
            validFail(xhr, status, error)
        })
        .always(function () {
        })

}