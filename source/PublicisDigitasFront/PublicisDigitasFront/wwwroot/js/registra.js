﻿
var fileBase64;

$(document).ready(function () {

    $("#registraReceta").submit(function (e) {
        e.preventDefault();
        var valores = $(this);

        RegistraReceta(valores);                
    });
});

function RegistraReceta(valores) {
    var imagenExiste = true;
    var receta = {
        RctName: $("#RctName").val(),
        RctDescription: $("#RctDescription").val(),
        RctCooking: $("#RctCooking").val(),
        RctImage: $("#RctImage").val()
    };

    $.ajax({
        url: receta.RctImage,
        type: 'HEAD',
        async:false
    }).done(function (respuesta) {
    })
        .fail(function (xhr, status, error) {
            sweetalerts('Advertencia', 'La imagen ingresada no existe', 'warning', 'Aceptar');
            imagenExiste = false;
        })
        .always(function () {
        })

    if (imagenExiste) {
        $.ajax({
            url: rutaServicio + "CookingRecipes/Create",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            async: true,
            data: JSON.stringify(receta),
            headers: {
                accept: "application/json",
                "Access-Control-Allow-Origin": "*",
                "Authorization": "Bearer " + localStorage.getItem("BearerToken")
            }
        })
            .done(function (respuesta) {
                sweetalertRedirect('PublicDigital', "Registro exitosamente", 'success', 'Aceptar', '../Administra/Informacion');
            })
            .fail(function (xhr, status, error) {
                validFail(xhr, status, error)
            })
            .always(function () {
            })
    }
   

}

