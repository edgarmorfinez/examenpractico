﻿
const rutaServicio = "http://localhost:5600/";
const inicio = document.getElementById("inicioId");
const iniciarSesion = document.getElementById("iniSesionId");
const cerrarSesion = document.getElementById("cerSesionId");
const bienvenida = document.getElementById("bienvenidaId");
const administrador = document.getElementById("administradorId");


$(document).ready(function () {

    let bearerToken = localStorage.getItem("BearerToken");
    if (bearerToken) {
        iniciarSesion.classList.add('d-none');
        administrador.classList.remove('d-none');
        cerrarSesion.classList.remove('d-none');
        bienvenida.innerHTML = "Bienvenido " + localStorage.getItem("userName");
    } else {
        iniciarSesion.classList.remove('d-none');
        administrador.classList.add('d-none');
        cerrarSesion.classList.add('d-none');
        bienvenida.innerHTML = "Bienvenido";
    }

    $('#cerSesionId').click(function () {
        localStorage.setItem("BearerToken", "")
        localStorage.setItem("userName", "")

        window.location = '../Index';
    });

});


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function sweetalerts(tit, tex, ico, butt) {
    swal({
        title: tit,
        text: tex,
        icon: ico,
        button: 'Aceptar',
    });
}

function sweetalertRedirect(tit, tex, ico, butt, ruta) {
    swal({
        title: tit,
        text: tex,
        icon: ico,
        button: 'Aceptar',
    }).then((value) => {
        window.location = ruta;
        }
    );
}

function validFail(xhr, status, error) {
    if (error == "Unauthorized") {
        localStorage.setItem("BearerToken", "")
        sweetalertRedirect('No Autorizado', 'Credenciales invalidas o tiempo agostado \n Volver a iniciar session', 'warning', 'Aceptar', '../Login');
    } else {
        if (typeof xhr.responseJSON == 'string') {
            sweetalerts("Advertencia", xhr.responseJSON, 'warning', 'Aceptar');
        } else {
            sweetalerts('Error', "Se produjo un error interno", 'error', 'Aceptar');
        }
    }
}